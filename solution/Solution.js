const fs = require("fs").promises;
const path = require("path");

class Solution {
  constructor() {
    this._className = this.constructor.name;

    this.loadInput(this._className)
      .then(this.solve.bind(this))
      .catch((err) => {
        throw err;
      });
  }
  async loadInput(input) {
    const filename = path.resolve(__dirname, "../input", input + ".txt");
    try {
      return await fs.readFile(filename, "utf8");
    } catch (err) {
      throw new Error("Input file can't be loaded: " + filename);
    }
  }

  async solveTimeWrapper(func, input) {
    const startTime = process.hrtime.bigint();
    const solution = await func.bind(this)(input);
    return {
      duration: "(" + (process.hrtime.bigint() - startTime) / 1000000n + "ms)",
      solution,
    };
  }

  async solve(input) {
    console.log("Solving", this._className);

    const silver = await this.solveTimeWrapper(this.solveSilver, input);
    const gold = await this.solveTimeWrapper(this.solveGold, input);

    console.log("[*1]", silver.solution, silver.duration);
    console.log("[*2]", gold.solution, gold.duration);
  }

  async solveSilver(input) {
    return "Not solved Yet";
  }

  async solveGold(input) {
    return "Not solved Yet";
  }
}

module.exports = Solution;
