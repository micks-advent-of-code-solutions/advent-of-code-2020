const Solution = require("./Solution");

const parseInput = (input) => input.split("\n");

const operations = {
  "+": (a, b) => a + b,
  "*": (a, b) => a * b,
};

const solveSimplePrecedence = (exp) => {
  let splitExp = exp.split(" ");

  Object.entries(operations).forEach(([op, func]) => {
    while (true) {
      const opPosition = splitExp.indexOf(op);
      if (opPosition === -1) {
        break;
      }
      const result = func(
        BigInt(splitExp[opPosition - 1]),
        BigInt(splitExp[opPosition + 1])
      );
      splitExp = splitExp
        .slice(0, opPosition - 1)
        .concat(result)
        .concat(splitExp.slice(opPosition + 2));
    }
  });

  return splitExp[0];
};

const solveSimple = (exp) => {
  let splitExp = exp.split(" ");

  let value = BigInt(splitExp.shift());

  while (splitExp.length > 0) {
    let op = splitExp.shift();
    let right = BigInt(splitExp.shift());
    value = operations[op](value, right);
  }

  return value;
};

const Solver = (simpleSolver) => (exp) => {
  let opening = exp.indexOf("(");
  let closing = exp.indexOf(")");
  if (opening === -1 && closing === -1) {
    return simpleSolver(exp);
  } else {
    while (exp.indexOf("(") > -1) {
      let start = exp.indexOf("(");
      let end = exp.length;
      for (let i = opening; i < exp.length; i++) {
        if (exp[i] === "(") {
          start = i;
          continue;
        }
        if (exp[i] === ")") {
          end = i;
          break;
        }
      }
      exp = exp.replace(
        exp.substring(start, end + 1),
        simpleSolver(exp.substring(start + 1, end))
      );
    }
    return simpleSolver(exp);
  }
};

module.exports = class Day18 extends (
  Solution
) {
  async solveSilver(input) {
    const solve = Solver(solveSimple);
    return parseInput(input)
      .map((exp) => solve(exp))
      .reduce(operations["+"]);
  }

  async solveGold(input) {
    const solve = Solver(solveSimplePrecedence);
    return parseInput(input)
      .map((exp) => solve(exp))
      .reduce(operations["+"]);
  }
};
