const Solution = require("./Solution");

const parseInput = (input) => {
  const [timestamp, lineString] = input.split("\n");
  const buslines = lineString
    .split(",")
    .filter((x) => x !== "x")
    .map((x) => parseInt(x));

  return { timestamp, buslines };
};

const parseInputPart2 = (input) => {
  const [, lineString] = input.split("\n");
  const buslines = lineString
    .split(",")
    .map((x, i) => [i, parseInt(x)])
    .filter(([, x]) => !isNaN(x));
  return buslines;
};

module.exports = class Day13 extends (
  Solution
) {
  async solveSilver(input) {
    const { buslines, timestamp } = parseInput(input);
    const nextBuses = buslines.map(
      (line) => Math.ceil(timestamp / line) * line
    );
    const nextBus = Math.min(...nextBuses);
    const nextBusId = buslines[nextBuses.indexOf(nextBus)];
    const waitTime = nextBus - timestamp;
    return nextBusId * waitTime;
  }

  async solveGold(input) {
    const buslines = parseInputPart2(input).map((line) =>
      line.map((x) => BigInt(x))
    );
    let time = 0n;
    let step = buslines[0][1];
    let busToCheck = 1;
    let bus = buslines[busToCheck];

    const lines = buslines.length;

    while (busToCheck < lines) {
      time += step;
      console.log(
        buslines
          .map(([offset, line]) => ((time + offset) % line === 0n ? "X" : "."))
          .join(""),
        time,
        step
      );
      if ((time + bus[0]) % bus[1] === 0n) {
        step = buslines
          .slice(0, busToCheck + 1)
          .map(([, line]) => line)
          .reduce((a, b) => a * b);
        busToCheck++;
        bus = buslines[busToCheck];
      }
    }
    return time;
  }
};
