const Solution = require("./Solution");

const boardingToBinary = (boarding) =>
  parseInt(boarding.replace(/[FL]/g, 0).replace(/[BR]/g, 1), 2);

module.exports = class Day5 extends (
  Solution
) {
  async solveSilver(input) {
    return Math.max(...input.split("\n").map((b) => boardingToBinary(b)));
  }

  async solveGold(input) {
    const boardingIds = input
      .split("\n")
      .map((b) => boardingToBinary(b))
      .sort((a, b) => a - b);
    const minBoundary = Math.min(...boardingIds);
    const maxBoundary = Math.max(...boardingIds);

    for (let i = minBoundary; i < maxBoundary; i++) {
      if (boardingIds[i + 1] - boardingIds[i] > 1) {
        return boardingIds[i] + 1;
      }
    }
  }
};
