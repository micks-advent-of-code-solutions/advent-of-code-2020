const Solution = require("./Solution");

const arrayHasSum = (arr, sum) => {
  const length = arr.length;
  for (let lower = 0; lower < length - 1; lower++) {
    for (let upper = length - 1; upper > 0; upper--) {
      if (upper === lower) {
        continue;
      }
      if (arr[lower] + arr[upper] === sum) {
        return true;
      }
    }
  }
  return false;
};

module.exports = class Day9 extends (
  Solution
) {
  async solveSilver(input) {
    const numbers = input.split("\n").map((n) => parseInt(n));

    const preamble = 25;
    const range = 25;

    for (let i = preamble + range; i < numbers.length; i++) {
      const current = numbers[i];
      const previous = numbers.slice(i - range, i);

      if (!arrayHasSum(previous, current)) {
        return current;
      }
    }
  }

  async solveGold(input) {
    const numbers = input.split("\n").map((n) => parseInt(n));
    const numberToFind = await this.solveSilver(input);

    for (let start = 0; start < numbers.length; start++) {
      let sum = 0;
      for (let end = start; end < numbers.length; end++) {
        sum += numbers[end];
        if (sum === numberToFind) {
          const range = numbers.slice(start, end + 1);
          return Math.min(...range) + Math.max(...range);
        }
      }
    }
  }
};
