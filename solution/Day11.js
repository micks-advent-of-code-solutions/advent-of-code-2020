const Solution = require("./Solution");

function GameMap(input) {
  const grid = input.split("\n").map((line) => line.split(""));
  const width = grid[0].length;
  const height = grid.length;

  const coord = (x, y) => {
    if (!inBounds(x, y)) {
      return undefined;
    }
    return grid[y][x % width];
  };
  const setCoord = (x, y, value) => {
    if (!inBounds(x, y)) {
      return;
    }
    grid[y][x % width] = value;
  };
  const inBounds = (x, y) => x >= 0 && y >= 0 && x < width && y < height;
  const toString = () => grid.map((line) => line.join("")).join("\n");

  return {
    width,
    height,
    coord,
    setCoord,
    inBounds,
    toString,
  };
}

const FLOOR = ".",
  EMPTY_SEAT = "L",
  OCCUPIED_SEAT = "#";

const partOneRule = (map, x, y) => {
  let seat = map.coord(x, y);

  if (seat === FLOOR) {
    return FLOOR;
  }
  const seatsAround = [];
  for (let xs = -1; xs <= 1; xs++) {
    for (let ys = -1; ys <= 1; ys++) {
      if (xs === 0 && ys === 0) {
        continue;
      }
      seatsAround.push(map.coord(x + xs, y + ys));
    }
  }
  if (
    seat === EMPTY_SEAT &&
    seatsAround.filter((s) => s === OCCUPIED_SEAT).length === 0
  ) {
    return OCCUPIED_SEAT;
  } else if (
    seat === OCCUPIED_SEAT &&
    seatsAround.filter((s) => s === OCCUPIED_SEAT).length >= 4
  ) {
    return EMPTY_SEAT;
  }
};

const lineOfSight = (map, x, y, xd, yd) => {
  let currX = x + xd,
    currY = y + yd;
  while (map.inBounds(currX, currY)) {
    map.coord(currX, currY);
    const seat = map.coord(currX, currY);

    if (seat !== FLOOR) {
      return seat;
    }

    currX += xd;
    currY += yd;
  }
};

const partTwoRule = (map, x, y) => {
  let seat = map.coord(x, y);

  if (seat === FLOOR) {
    return FLOOR;
  }

  const seatsAround = [];
  for (let xs = -1; xs <= 1; xs++) {
    for (let ys = -1; ys <= 1; ys++) {
      if (xs === 0 && ys === 0) {
        continue;
      }
      seatsAround.push(lineOfSight(map, x, y, xs, ys));
    }
  }
  if (
    seat === EMPTY_SEAT &&
    seatsAround.filter((s) => s === OCCUPIED_SEAT).length === 0
  ) {
    return OCCUPIED_SEAT;
  } else if (
    seat === OCCUPIED_SEAT &&
    seatsAround.filter((s) => s === OCCUPIED_SEAT).length >= 5
  ) {
    return EMPTY_SEAT;
  }
};

const simulate = (map, ruleFunc) => {
  const mapAfter = GameMap(map.toString());
  for (let x = 0; x < map.width; x++) {
    for (let y = 0; y < map.height; y++) {
      const newState = ruleFunc(map, x, y);
      if (newState) {
        mapAfter.setCoord(x, y, newState);
      }
    }
  }
  return mapAfter;
};

const simulateUntilStable = (map, ruleFunc, showProgress = false) => {
  let begin = map,
    simulated = simulate(begin, ruleFunc),
    count = 1;

  if (showProgress) {
    console.log(count);
    console.log(
      begin.toString() + "\n-----------\n" + simulated.toString() + "\n\n"
    );
  }
  while (begin.toString() !== simulated.toString()) {
    count++;

    const start = GameMap(simulated.toString());
    simulated = simulate(simulated, ruleFunc);
    begin = start;
    if (showProgress) {
      console.log(count);
      console.log(
        begin.toString() + "\n-----------\n" + simulated.toString() + "\n\n"
      );
    }
  }
  return simulated;
};

module.exports = class Day11 extends (
  Solution
) {
  async solveSilver(input) {
    return simulateUntilStable(GameMap(input), partOneRule)
      .toString()
      .split("")
      .filter((s) => s === OCCUPIED_SEAT).length;
  }

  async solveGold(input) {
    return simulateUntilStable(GameMap(input), partTwoRule)
      .toString()
      .split("")
      .filter((s) => s === OCCUPIED_SEAT).length;
  }
};
