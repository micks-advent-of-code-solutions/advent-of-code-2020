const Solution = require("./Solution");

const parseInput = (input) => {
  const [ruleBlock, tests] = input.split("\n\n").map((b) => b.split("\n"));
  const rules = Object.fromEntries(ruleBlock.map((rule) => rule.split(": ")));

  return {
    tests,
    rules,
  };
};
const maxDepth = 10;
const resolveRule = (rules, ruleID, level = 0) => {
  const rule = rules[ruleID];
  if (rule.startsWith('"')) {
    return rule.split('"')[1];
  }
  if (rule.includes("|")) {
    return (
      "(" +
      rule
        .split(" | ")
        .map((part) =>
          part
            .split(" ")
            .map((id) => {
              if (id === ruleID) {
                // Make sure we don't loop infinitly by stopping the recursion at maxDepth
                if (level < maxDepth) {
                  return resolveRule(rules, id, level + 1);
                } else {
                  return "";
                }
              } else {
                return resolveRule(rules, id);
              }
            })
            .join("")
        )
        .join("|") +
      ")"
    );
  }
  return rule
    .split(" ")
    .map((id) => resolveRule(rules, id))
    .join("");
};

module.exports = class Day19 extends (
  Solution
) {
  async solveSilver(input) {
    const { tests, rules } = parseInput(input);
    const rule = resolveRule(rules, 0);
    const ruleRegex = new RegExp("^" + rule + "$");
    return tests.filter((test) => {
      return ruleRegex.test(test);
    }).length;
  }

  async solveGold(input) {
    const { tests, rules } = parseInput(input);
    rules[8] = "42 | 42 8";
    rules[11] = "42 31 | 42 11 31";
    const rule = resolveRule(rules, 0);
    console.log(rule);
    const ruleRegex = new RegExp("^" + rule + "$");
    return tests.filter((test) => {
      return ruleRegex.test(test);
    }).length;
  }
};
