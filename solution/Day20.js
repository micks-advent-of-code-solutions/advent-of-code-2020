const Solution = require("./Solution");

const parseInput = (input) =>
  Object.fromEntries(
    input.split("\n\n").map((tile) => {
      const lines = tile.split("\n");
      const tileId = lines.shift().match(/Tile (\d+):/)[1];

      const borders = {
        top: lines[0],
        bottom: lines[lines.length - 1],
        left: lines.map((line) => line[0]).join(""),
        right: lines.map((line) => line[line.length - 1]).join(""),
      };

      return [tileId, { lines, borders }];
    })
  );

module.exports = class Day20 extends (
  Solution
) {
  async solveSilver(input) {
    const tiles = parseInput(input);

    const tilesWithNeighbors = Object.fromEntries(
      Object.entries(tiles).map(([tileID, tile]) => [
        tileID,
        {
          ...tile,
          neighbors: Object.values(tile.borders).filter((border) => {
            return Object.entries(tiles).some(([testID, testTile]) => {
              if (tileID === testID) {
                return false;
              }
              return Object.values(testTile.borders).some((testBorder) => {
                return (
                  border === testBorder ||
                  testBorder.split("").reverse().join("") === border
                );
              });
            });
          }).length,
        },
      ])
    );

    const tileIdsWithTwoNeighbors = Object.entries(tilesWithNeighbors)
      .filter(([tileID, tile]) => tile.neighbors === 2)
      .map(([tileID]) => BigInt(tileID));

    return tileIdsWithTwoNeighbors.reduce((a, b) => a * b);
  }

  async solveGold(input) {
    return "Not solved Yet!";
  }
};
