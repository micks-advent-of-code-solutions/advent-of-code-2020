const Solution = require("./Solution");

const parseInput = (input) => {
  const [rules, ownTicket, tickets] = input
    .split("\n\n")
    .map((i) => i.split("\n"));
  return {
    rules: rules.map((rule) => {
      const data = /([\w ]+): (\d+)-(\d+) or (\d+)-(\d+)/.exec(rule);
      return {
        name: data[1],
        ranges: [
          [parseInt(data[2]), parseInt(data[3])],
          [parseInt(data[4]), parseInt(data[5])],
        ],
      };
    }),
    ownTicket: ownTicket[1].split(",").map((n) => parseInt(n)),
    tickets: tickets
      .slice(1)
      .map((ticket) => ticket.split(",").map((n) => parseInt(n))),
  };
};

module.exports = class Day16 extends (
  Solution
) {
  async solveSilver(input) {
    const { rules, ownTicket, tickets } = parseInput(input);

    const flatRules = rules.map((rule) => rule.ranges).flat();
    const invalidNumbers = tickets
      .flat()
      .filter(
        (number) =>
          !flatRules.some(([from, to]) => number >= from && number <= to)
      );

    return invalidNumbers.reduce((a, b) => a + b, 0);
  }

  async solveGold(input) {
    const { rules, ownTicket, tickets } = parseInput(input);

    const flatRules = rules.map((rule) => rule.ranges).flat();
    const validTickets = tickets.filter((ticket) =>
      ticket.every((number) =>
        flatRules.some(([from, to]) => number >= from && number <= to)
      )
    );

    const ticketRows = [...new Array(validTickets[0].length)].map((x, row) => {
      return validTickets.map((ticket) => ticket[row]);
    });

    const assignedRules = {};

    for (let row = 0; row < ticketRows.length; row++) {
      rules.forEach((rule) => {
        if (
          ticketRows[row].every((number) =>
            rule.ranges.some(([from, to]) => number >= from && number <= to)
          )
        ) {
          const rules = assignedRules[rule.name] ?? new Set();
          rules.add(row);
          assignedRules[rule.name] = rules;
        }
      });
    }

    const foundRules = {};

    while (
      Math.max(...Object.values(assignedRules).map((rowSet) => rowSet.size)) > 0
    ) {
      const ruleToEliminiate = [
        ...Object.entries(assignedRules).find(([, rows]) => rows.size === 1),
      ];
      const rowToEliminiate = [...ruleToEliminiate[1].values()][0];

      foundRules[ruleToEliminiate[0]] = rowToEliminiate;

      Object.values(assignedRules).forEach((rowSet) =>
        rowSet.delete(rowToEliminiate)
      );
    }

    return Object.entries(foundRules)
      .filter(([name]) => name.startsWith("departure"))
      .map(([, pos]) => ownTicket[pos])
      .reduce((a, b) => BigInt(a) * BigInt(b));
  }
};
