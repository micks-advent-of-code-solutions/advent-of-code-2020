const Solution = require("./Solution");

const GameMap = (input) => {
  const grid = input.split("\n").map((line) => line.split(""));
  const width = grid[0].length;
  const height = grid.length;
  return { width, height, coord: (x, y) => grid[y][x % width] };
};

const treeFilter = (i) => i === "#";

module.exports = class Day3 extends (
  Solution
) {
  async solveSilver(input) {
    const map = GameMap(input);
    const path = [];
    for (let y = 0; y < map.height; y++) {
      path.push(map.coord(y * 3, y));
    }
    return path.filter(treeFilter).length;
  }

  async solveGold(input) {
    const map = GameMap(input);
    const paths = { 1: [], 3: [], 5: [], 7: [], halbi: [] };
    for (let y = 0; y < map.height; y++) {
      paths[1].push(map.coord(y * 1, y));
      paths[3].push(map.coord(y * 3, y));
      paths[5].push(map.coord(y * 5, y));
      paths[7].push(map.coord(y * 7, y));
      if (y % 2 === 0) {
        paths.halbi.push(map.coord(y / 2, y));
      }
    }

    return Object.values(paths)
      .map((path) => path.filter(treeFilter).length)
      .reduce((a, b) => a * b);
  }
};
