const Solution = require("./Solution");

const parseInstructions = (input) =>
  input
    .split("\n")
    .map((l) => ({ action: l.substr(0, 1), value: parseInt(l.substr(1)) }));

class Ship {
  x = 0;
  y = 0;
  direction = 90;

  actionMapping = {
    N: this.north,
    S: this.south,
    E: this.east,
    W: this.west,
    F: this.forward,
    R: this.right,
    L: this.left,
  };

  directionMapping = {
    0: this.north,
    90: this.east,
    180: this.south,
    270: this.west,
  };

  north(steps) {
    this.y -= steps;
  }

  south(steps) {
    this.y += steps;
  }

  east(steps) {
    this.x += steps;
  }

  west(steps) {
    this.x -= steps;
  }

  forward(steps) {
    this.directionMapping[this.direction].bind(this)(steps);
  }

  right(degrees) {
    this.direction = (this.direction + degrees) % 360;
  }

  left(degrees) {
    this.direction = (this.direction - degrees + 360) % 360;
  }

  applyInstruction(action, value) {
    this.actionMapping[action].bind(this)(value);
  }
}

const Ship2 = () => {
  let waypoint = { x: 10, y: -1 };
  let ship = { x: 0, y: 0 };

  const commands = {
    N: (value) => (waypoint.y -= value),
    S: (value) => (waypoint.y += value),
    E: (value) => (waypoint.x += value),
    W: (value) => (waypoint.x -= value),
    F: (value) => {
      ship.x += waypoint.x * value;
      ship.y += waypoint.y * value;
    },
    R: (angle) =>
      ({
        0: () => {},
        90: () => {
          waypoint = { x: -waypoint.y, y: waypoint.x };
        },
        180: () => {
          waypoint = { x: -waypoint.x, y: -waypoint.y };
        },
        270: () => {
          waypoint = { x: waypoint.y, y: -waypoint.x };
        },
      }[angle]()),
    L: (angle) =>
      ({
        0: () => {},
        90: () => {
          waypoint = { x: waypoint.y, y: -waypoint.x };
        },
        180: () => {
          waypoint = { x: -waypoint.x, y: -waypoint.y };
        },
        270: () => {
          waypoint = { x: -waypoint.y, y: waypoint.x };
        },
      }[angle]()),
  };

  const applyInstruction = (action, value) => {
    commands[action](value);
  };

  return {
    get waypoint() {
      return waypoint;
    },
    get ship() {
      return ship;
    },
    applyInstruction,
  };
};

module.exports = class Day12 extends (
  Solution
) {
  async solveSilver(input) {
    const instructions = parseInstructions(input);
    const ship = new Ship();
    instructions.forEach(({ action, value }) => {
      ship.applyInstruction(action, value);
    });
    return Math.abs(ship.x) + Math.abs(ship.y);
  }

  async solveGold(input) {
    const instructions = parseInstructions(input);
    const ship = Ship2();
    instructions.forEach(({ action, value }) => {
      ship.applyInstruction(action, value);
    });
    return Math.abs(ship.ship.x) + Math.abs(ship.ship.y);
  }
};
