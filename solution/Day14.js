const Solution = require("./Solution");

const parseInput = (input) =>
  input
    .split("\n")
    .map(
      (line) =>
        /(?<instr>mask|mem)(\[(?<addr>\d+)\])? = (?<param>.*)/.exec(line).groups
    );

const Computer = (program) => {
  let positiveBitmask = null;
  let negativeBitmask = null;
  const memory = {};
  let pc = 0;

  const instructions = {
    mask(addr, param) {
      positiveBitmask = BigInt(
        parseInt(
          param
            .split("")
            .map((x) => (x === "1" ? 1 : 0))
            .join(""),
          2
        )
      );

      negativeBitmask = BigInt(
        parseInt(
          param
            .split("")
            .map((x) => (x === "0" ? 0 : 1))
            .join(""),
          2
        )
      );
    },
    mem(addr, param) {
      let number = BigInt(param);
      memory[addr] = (number | positiveBitmask) & negativeBitmask;
    },
  };

  const step = () => {
    if (pc === program.length) {
      return false;
    }
    const { instr, addr, param } = program[pc];
    instructions[instr](addr, param);
    pc++;
    return { memory };
  };

  return {
    memory,
    step,
  };
};

const ComputerV2 = (program) => {
  let positiveBitmask = null;
  let fluctuatingBits = null;
  const memory = {};
  let pc = 0;

  const calculateAddresses = (addr) => {
    const addresses = [];
    const variants = BigInt(2 ** fluctuatingBits.length);
    for (let i = 0n; i < variants; i++) {
      let changedAddr = addr;
      fluctuatingBits.forEach((bit, pos) => {
        const shouldBeSet = i & (1n << BigInt(pos));
        if (shouldBeSet) {
          changedAddr = changedAddr | (1n << bit);
        } else {
          changedAddr = changedAddr & ~(1n << bit);
        }
      });
      addresses.push(changedAddr);
    }
    return addresses;
  };

  const instructions = {
    mask(addr, param) {
      fluctuatingBits = param
        .split("")
        .reverse()
        .map((x, i) => [i, x === "X"])
        .filter(([, x]) => x)
        .map(([i]) => BigInt(i));

      positiveBitmask = BigInt(
        parseInt(
          param
            .split("")
            .map((x) => (x === "1" ? 1 : 0))
            .join(""),
          2
        )
      );
    },
    mem(addr, param) {
      let number = BigInt(param);
      const addresses = calculateAddresses(BigInt(addr) | positiveBitmask);
      addresses.forEach((addr) => {
        memory[addr] = number;
      });
    },
  };

  const step = () => {
    if (pc === program.length) {
      return false;
    }
    const { instr, addr, param } = program[pc];
    instructions[instr](addr, param);
    pc++;
    return { memory };
  };

  return {
    memory,
    step,
  };
};

module.exports = class Day14 extends (
  Solution
) {
  async solveSilver(input) {
    const program = parseInput(input);
    const computer = Computer(program);
    while (computer.step()) {}
    return Object.values(computer.memory).reduce((a, b) => a + b);
  }

  async solveGold(input) {
    const program = parseInput(input);
    const computer = ComputerV2(program);
    while (computer.step()) {}
    return Object.values(computer.memory).reduce((a, b) => a + b);
  }
};
