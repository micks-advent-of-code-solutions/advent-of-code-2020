const Solution = require("./Solution");

const parsePassports = (input) =>
  input
    .split("\n\n")
    .map((p) => p.trim().replace(/\n/g, " "))
    .map((p) =>
      Object.fromEntries(p.split(" ").map((attr) => attr.split(":")))
    );

const numberRangeValidator = (begin, end) => (value) => {
  const parsed = parseInt(value);
  return parsed >= begin && parsed <= end;
};

const regexValidator = (regex) => (value) => regex.test(value);

module.exports = class Day4 extends (
  Solution
) {
  async solveSilver(input) {
    const passports = parsePassports(input);
    const required = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"];
    return passports.filter((p) =>
      required.every((field) => p.hasOwnProperty(field))
    ).length;
  }

  async solveGold(input) {
    const passports = parsePassports(input);
    const validators = {
      byr: numberRangeValidator(1920, 2002),
      iyr: numberRangeValidator(2010, 2020),
      eyr: numberRangeValidator(2020, 2030),
      hgt: (value) => {
        if (value.endsWith("cm")) {
          return numberRangeValidator(150, 193)(value);
        }
        if (value.endsWith("in")) {
          return numberRangeValidator(59, 76)(value);
        }
        return false;
      },
      hcl: regexValidator(/^#[0-9a-f]{6}$/),
      ecl: regexValidator(/^(amb|blu|brn|gry|grn|hzl|oth)$/),
      pid: regexValidator(/^[\d]{9}$/),
    };
    return passports.filter((p) =>
      Object.entries(validators).every(([field, validatorFunc]) => {
        try {
          return p.hasOwnProperty(field) && validatorFunc(p[field]);
        } catch (e) {
          return false;
        }
      })
    ).length;
  }
};
