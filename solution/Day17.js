const Solution = require("./Solution");

const CubeOfLife = (input) => {
  let map = new Set();

  const setState = (map, x, y, z, active) =>
    active ? map.add(x + "/" + y + "/" + z) : map.delete(x + "/" + y + "/" + z);
  const getState = (map, x, y, z) => map.has(x + "/" + y + "/" + z);

  const getBounds = (map) => {
    const coordsByAxis = [[], [], []];
    const coords = [...map.values()].map((entry) =>
      entry.split("/").map((n) => parseInt(n))
    );
    coords.forEach((coord) => coord.forEach((c, i) => coordsByAxis[i].push(c)));
    return coordsByAxis.map((axis) => [Math.min(...axis), Math.max(...axis)]);
  };

  const getNeighbors = (map, x, y, z) => {
    const neighbors = [];
    for (let sx = x - 1; sx <= x + 1; sx++) {
      for (let sy = y - 1; sy <= y + 1; sy++) {
        for (let sz = z - 1; sz <= z + 1; sz++) {
          if (sx === x && sy === y && sz === z) {
            continue;
          }
          neighbors.push(getState(map, sx, sy, sz));
        }
      }
    }
    return neighbors;
  };

  input.split("\n").forEach((line, y) => {
    line.split("").forEach((active, x) => {
      if (active === "#") {
        setState(map, x, y, 0, true);
      }
    });
  });

  const step = () => {
    const [xBounds, yBounds, zBounds] = getBounds(map);
    const newMap = new Set();

    for (let x = xBounds[0] - 1; x <= xBounds[1] + 1; x++) {
      for (let y = yBounds[0] - 1; y <= yBounds[1] + 1; y++) {
        for (let z = zBounds[0] - 1; z <= zBounds[1] + 1; z++) {
          const active = getState(map, x, y, z);
          const activeNeigbors = getNeighbors(map, x, y, z).filter((x) => x)
            .length;

          if (
            (active && (activeNeigbors === 2 || activeNeigbors === 3)) ||
            (!active && activeNeigbors === 3)
          ) {
            setState(newMap, x, y, z, true);
          }
        }
      }
    }

    map = newMap;
  };

  const printMap = () => {
    const [xBounds, yBounds, zBounds] = getBounds(map);

    for (let z = zBounds[0]; z <= zBounds[1]; z++) {
      console.log(`z=${z}`);
      for (let y = yBounds[0]; y <= yBounds[1]; y++) {
        let row = [];
        for (let x = xBounds[0]; x <= xBounds[1]; x++) {
          row.push(getState(map, x, y, z) ? "#" : ".");
        }
        console.log(row.join(""));
      }
      console.log();
    }
  };

  return {
    get map() {
      return map;
    },
    step,
    printMap,
    get activeCubes() {
      return map.size;
    },
  };
};

// copypasta of CubeOfLife with coord w added
const CubeOfLife4 = (input) => {
  let map = new Set();

  // const decodePosition = (position) =>
  //   position.split("/").map((n) => parseInt(n));
  // const encodePosition = (x, y, z, w) => x + "/" + y + "/" + z + "/" + w;

  const decodePosition = (position) => {
    return [
      (position >> 24) - 127,
      ((position >> 16) & 0xff) - 127,
      ((position >> 8) & 0xff) - 127,
      (position & 0xff) - 127,
    ];
  };
  const encodePosition = (x, y, z, w) =>
    ((x + 127) << 24) | ((y + 127) << 16) | ((z + 127) << 8) | (w + 127);

  const setState = (map, x, y, z, w) => map.add(encodePosition(x, y, z, w));
  const getState = (map, x, y, z, w) => map.has(encodePosition(x, y, z, w));

  const getBounds = (map) => {
    const coordsByAxis = [[], [], [], []];
    const coords = [...map.values()].map((entry) => decodePosition(entry));
    coords.forEach((coord) => coord.forEach((c, i) => coordsByAxis[i].push(c)));
    return coordsByAxis.map((axis) => [Math.min(...axis), Math.max(...axis)]);
  };

  input.split("\n").forEach((line, y) => {
    line.split("").forEach((active, x) => {
      if (active === "#") {
        setState(map, x, y, 0, 0);
      }
    });
  });

  const step = () => {
    const newMap = new Set();

    const neighborsToCheck = new Set();

    map.forEach((entry) => {
      const [x, y, z, w] = decodePosition(entry);

      let activeNeigbors = 0;
      for (let sx = x - 1; sx <= x + 1; sx++) {
        for (let sy = y - 1; sy <= y + 1; sy++) {
          for (let sz = z - 1; sz <= z + 1; sz++) {
            for (let sw = w - 1; sw <= w + 1; sw++) {
              if (sx === x && sy === y && sz === z && sw === w) {
                continue;
              }
              const state = getState(map, sx, sy, sz, sw);
              if (state) {
                activeNeigbors++;
              } else {
                setState(neighborsToCheck, sx, sy, sz, sw);
              }
            }
          }
        }
      }
      if (activeNeigbors === 2 || activeNeigbors === 3) {
        newMap.add(entry);
      }
    });

    neighborsToCheck.forEach((entry) => {
      const [x, y, z, w] = decodePosition(entry);

      let activeNeigbors = 0;
      for (let sx = x - 1; sx <= x + 1; sx++) {
        for (let sy = y - 1; sy <= y + 1; sy++) {
          for (let sz = z - 1; sz <= z + 1; sz++) {
            for (let sw = w - 1; sw <= w + 1; sw++) {
              if (sx === x && sy === y && sz === z && sw === w) {
                continue;
              }
              const state = getState(map, sx, sy, sz, sw);
              if (state) {
                activeNeigbors++;
              }
            }
          }
        }
      }
      if (activeNeigbors === 3) {
        newMap.add(entry);
      }
    });

    map = newMap;
  };

  const printMap = () => {
    const [xBounds, yBounds, zBounds, wBounds] = getBounds(map);

    for (let w = wBounds[0]; w <= wBounds[1]; w++) {
      for (let z = zBounds[0]; z <= zBounds[1]; z++) {
        console.log(`z=${z} w=${w}`);
        for (let y = yBounds[0]; y <= yBounds[1]; y++) {
          let row = [];
          for (let x = xBounds[0]; x <= xBounds[1]; x++) {
            row.push(getState(map, x, y, z, w) ? "#" : ".");
          }
          console.log(row.join(""));
        }
        console.log();
      }
    }
  };

  return {
    get map() {
      return map;
    },
    step,
    printMap,
    get activeCubes() {
      return map.size;
    },
  };
};

module.exports = class Day17 extends (
  Solution
) {
  async solveSilver(input) {
    const cube = CubeOfLife(input);
    for (let i = 0; i < 6; i++) {
      cube.step();
    }
    //cube.printMap();
    return cube.activeCubes;
  }

  async solveGold(input) {
    const cube = CubeOfLife4(input);
    for (let i = 0; i < 6; i++) {
      //const startTime = process.hrtime.bigint();
      cube.step();
      //console.log(i, (process.hrtime.bigint() - startTime) / 1000000n + "ms");
    }
    //cube.printMap();
    return cube.activeCubes;
  }
};
