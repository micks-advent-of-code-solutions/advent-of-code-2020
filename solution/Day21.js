const Solution = require("./Solution");

const parseInput = (input) => {
  return input.split("\n").map((line) => {
    const { ingredients, allergens } = line.match(
      /^(?<ingredients>[\w ]+?) \(contains (?<allergens>[\w, ]+)\)$/
    ).groups;
    return {
      ingredients: ingredients.split(" "),
      allergens: allergens.split(", "),
    };
  });
};

module.exports = class Day21 extends (
  Solution
) {
  async solveSilver(input) {
    return parseInput(input);
  }

  async solveGold(input) {
    return "Not solved Yet!";
  }
};
