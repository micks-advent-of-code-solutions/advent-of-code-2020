const Solution = require("./Solution");

const findPaths = (connections) => {
  const track = (node) => {
    const currentConnections = connections[node];
    if (pathsFromHere[node]) {
      return pathsFromHere[node];
    }
    if (currentConnections.length === 0) {
      pathsFromHere[node] = 1;
      return 1;
    } else {
      const paths = currentConnections.map(track).reduce((a, b) => a + b);
      pathsFromHere[node] = paths;
      return paths;
    }
  };

  const pathsFromHere = {};

  return track(0);
};

module.exports = class Day10 extends (
  Solution
) {
  async solveSilver(input) {
    const sortedAdapters = input
      .split("\n")
      .map((n) => parseInt(n))
      .sort((a, b) => a - b);

    const distances = sortedAdapters.map((value, i, arr) => {
      return value - (arr[i - 1] ?? 0);
    });

    return (
      distances.filter((i) => i === 1).length *
      (distances.filter((i) => i === 3).length + 1)
    );
  }

  async solveGold(input) {
    const sortedAdapters = input
      .split("\n")
      .map((n) => parseInt(n))
      .sort((a, b) => a - b);
    sortedAdapters.unshift(0);

    const connections = Object.fromEntries(
      sortedAdapters.map((value, i, arr) => {
        const potentialConnections = arr
          .slice(i + 1, i + 4)
          .filter((candidate) => candidate - value <= 3);
        return [value, potentialConnections];
      })
    );

    return findPaths(connections);
  }
};
