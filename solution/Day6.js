const Solution = require("./Solution");

const getAnswers = (input) =>
  input
    .split("\r\n\r\n")
    .map((group) => new Set(group.replace(/\r\n/g, "").split("")));

const getAnswersPerPerson = (input) =>
  input
    .split("\r\n\r\n")
    .map((group) =>
      group.split("\r\n").map((person) => new Set(person.split("")))
    );

const getOverlap = (persons) => {
  const allAnswers = [...new Set(...persons)];
  return allAnswers.filter((answer) =>
    persons.every((person) => {
      return person.has(answer);
    })
  );
};

const sum = (a, b) => a + b;

module.exports = class Day6 extends (
  Solution
) {
  async solveSilver(input) {
    return getAnswers(input)
      .map((group) => group.size)
      .reduce(sum);
  }

  async solveGold(input) {
    return getAnswersPerPerson(input)
      .map(getOverlap)
      .map((group) => group.length)
      .reduce(sum);
  }
};
