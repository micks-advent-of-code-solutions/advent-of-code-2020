const Solution = require("./Solution");

const parseInput = (input) => {
  return input.split("\n").map((line) => {
    const [instruction, value] = line.split(" ");
    return {
      instruction,
      value: parseInt(value),
    };
  });
};

const Computer = (program) => {
  let pc, accumulator, running;
  restart();

  const instructions = {
    nop() {
      pc++;
    },
    acc(value) {
      accumulator += value;
      pc++;
    },
    jmp(value) {
      pc += value;
    },
  };

  function step(debug = false) {
    if (!running) {
      return;
    }
    const currentStep = program[pc];

    if (currentStep.isRun) {
      throw new Error(accumulator);
    }
    currentStep.isRun = true;
    instructions[currentStep.instruction](currentStep.value);
    if (pc >= program.length) {
      running = false;
    }
  }

  function restart() {
    pc = 0;
    accumulator = 0;
    running = true;
  }

  return {
    step,
    get pc() {
      return pc;
    },
    get accumulator() {
      return accumulator;
    },
    get running() {
      return running;
    },
    program,
  };
};

module.exports = class Day8 extends (
  Solution
) {
  async solveSilver(input) {
    const program = parseInput(input);

    const computer = Computer(program);

    try {
      while (computer.running) {
        computer.step();
      }
    } catch (err) {
      return computer.accumulator;
    }
  }

  async solveGold(input) {
    const program = parseInput(input);
    // Find all interesting instructions
    const interestingInstructions = program
      .map((inst, i) => ({
        pos: i,
        ...inst,
      }))
      .filter((i) => ["jmp", "nop"].includes(i.instruction));

    // Lets try changing them one by one until the program terminates :)
    for (let i = 0; i < interestingInstructions.length; i++) {
      const instructionToChange = interestingInstructions[i];
      const modifiedProgram = parseInput(input);
      modifiedProgram[instructionToChange.pos].instruction =
        instructionToChange.instruction === "nop" ? "jmp" : "nop";

      const computer = Computer(modifiedProgram);
      try {
        // Run the modified program
        while (computer.running) {
          computer.step();
        }
        // Program has terminated, this is the solution :)
        return computer.accumulator;
      } catch (err) {
        // That one created a infinite loop, not the solution :(
      }
    }
  }
};
