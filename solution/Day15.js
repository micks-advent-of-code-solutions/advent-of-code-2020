const Solution = require("./Solution");

const parseInput = (input) => input.split(",").map((x) => parseInt(x));

module.exports = class Day15 extends (
  Solution
) {
  async solveSilver(input) {
    const startingNumbers = parseInput(input);
    const lastHeard = new Map(
      startingNumbers
        .slice(0, startingNumbers.length - 1)
        .map((number, pos) => [number, pos + 1])
    );
    let lastNumber = startingNumbers[startingNumbers.length - 1];
    let numberToStore = lastNumber;
    for (let round = startingNumbers.length + 1; round < 2021; round++) {
      let lastHeardInRound = lastHeard.get(lastNumber);
      let numberToShout = 0;
      if (lastHeardInRound) {
        numberToShout = round - 1 - lastHeardInRound;
      }

      lastHeard.set(numberToStore, round - 1);

      lastNumber = numberToShout;
      numberToStore = numberToShout;
    }
    return lastNumber;
  }

  async solveGold(input) {
    const startingNumbers = parseInput(input);
    const lastHeard = new Map(
      startingNumbers
        .slice(0, startingNumbers.length - 1)
        .map((number, pos) => [number, pos + 1])
    );
    let lastNumber = startingNumbers[startingNumbers.length - 1];
    let numberToStore = lastNumber;
    for (let round = startingNumbers.length + 1; round < 30000001; round++) {
      let lastHeardInRound = lastHeard.get(lastNumber);
      let numberToShout = 0;
      if (lastHeardInRound) {
        numberToShout = round - 1 - lastHeardInRound;
      }

      lastHeard.set(numberToStore, round - 1);

      lastNumber = numberToShout;
      numberToStore = numberToShout;
    }
    return lastNumber;
  }
};
