const Solution = require("./Solution");

module.exports = class Day2 extends (
  Solution
) {
  parseLines(input) {
    const lines = input.split("\n");
    const passwords = lines.map((l) => {
      const {
        groups: { char, password, a, b },
      } = /(?<a>\d+)-(?<b>\d+) (?<char>\w): (?<password>\w+)/.exec(l);
      return { char, password, a: parseInt(a), b: parseInt(b) };
    });
    return passwords;
  }

  async solveSilver(input) {
    const passwords = this.parseLines(input);
    return passwords.filter((p) => {
      const occurances = p.password.matchAll(p.char);
      const amount = [...occurances].length;
      return amount >= p.a && amount <= p.b;
    }).length;
  }

  async solveGold(input) {
    const passwords = this.parseLines(input);
    return passwords.filter((p) => {
      return (
        (p.password[p.a - 1] === p.char) ^ (p.password[p.b - 1] === p.char)
      );
    }).length;
  }
};
