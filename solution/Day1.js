const Solution = require("./Solution");

module.exports = class Day1 extends (
  Solution
) {
  async solveSilver(input) {
    const numbers = input.split("\n").map((x) => parseInt(x));
    for (let x = 0; x < numbers.length; x++) {
      for (let y = 0; y < numbers.length - 1; y++) {
        if (numbers[x] + numbers[y] === 2020) {
          return numbers[x] * numbers[y];
        }
      }
    }
  }

  async solveGold(input) {
    const numbers = input.split("\n").map((x) => parseInt(x));
    for (let x = 0; x < numbers.length; x++) {
      for (let y = 0; y < numbers.length - 1; y++) {
        for (let z = 0; z < numbers.length - 2; z++) {
          if (numbers[x] + numbers[y] + numbers[z] === 2020) {
            return numbers[x] * numbers[y] * numbers[z];
          }
        }
      }
    }
  }
};
