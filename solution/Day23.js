const Solution = require("./Solution");

const parseInput = (input) => input.split("").map((n) => parseInt(n));

class LinkedRingNode {
  #ring = null;
  #value = null;

  constructor(ring, value, next = null) {
    ring._register(value, this);
    this.#ring = ring;
    this.#value = value;
    this.next = next;
  }
  appendValue(value) {
    const newNode = new LinkedRingNode(this.#ring, value, this.next);
    this.next = newNode;
    return newNode;
  }

  appendValues(values) {
    let lastNode = this;
    values.forEach((value) => {
      lastNode = lastNode.appendValue(value);
    });
    return lastNode;
  }

  get value() {
    return this.#value;
  }

  /**
   * Get values of all next node up to (including) endNode
   * @param {LinkedRingNode} endNode Last node to add to ValueList
   */
  getNextValuesSetTo(endNode) {
    let node = this.next;
    let values = new Set();

    while (node !== endNode.next) {
      values.add(node.value);
      node = node.next;
    }
    return values;
  }

  /**
   * Get values of all next node up to (including) endNode
   * @param {LinkedRingNode} endNode Last node to add to ValueList
   */
  getNextValuesTo(endNode) {
    let node = this.next;
    let values = [];

    while (node !== endNode.next) {
      values.push(node.value);
      node = node.next;
    }
    return values;
  }

  getNextNode(numberOfNodes = 1) {
    let node = this.next;
    for (let i = 1; i < numberOfNodes; i++) {
      node = node.next;
    }
    return node;
  }

  toString() {
    return `Node { value = ${this.value}, next = ${this.next.#value} }`;
  }
}

class LinkedRing {
  #first;
  #valueMap = new Map();

  constructor(values) {
    this.#first = new LinkedRingNode(this, values.shift());
    this.#first.next = this.#first;
    this.#first.appendValues(values);
  }

  _register(value, node) {
    if (this.#valueMap.has(value)) {
      throw new Error(
        `Values have to be unique, Node with ${value} already exists`
      );
    }
    this.#valueMap.set(value, node);
  }

  getRing() {
    const ring = [this.#first];
    let node = this.#first;
    while (node.next !== this.#first) {
      node = node.next;
      ring.push(node);
    }
    return ring;
  }

  getValues() {
    const ring = [this.#first.value];
    let node = this.#first;
    while (node.next !== this.#first) {
      node = node.next;
      ring.push(node.value);
    }
    return ring;
  }

  getNodeByValue(value) {
    return this.#valueMap.get(value);
  }

  /**
   * Moves a Ringsegment defined by ]startnode,endnode] (startNode is not part of the Segment!) after destinationNode
   * @param {LinkedRingNode} startNode Node after! which the Segment to be moved starts
   * @param {LinkedRingNode} endNode Node which ends Segment to be moved
   * @param {*} destinationNode Node after which the moving Segment is moved to
   */
  moveNodesAfter(startNode, endNode, destinationNode) {
    const newEndNode = destinationNode.next;
    destinationNode.next = startNode.next;
    startNode.next = endNode.next;
    endNode.next = newEndNode;
  }

  get first() {
    return this.#first;
  }
}

module.exports = class Day23 extends (
  Solution
) {
  async solveSilver(input) {
    const cups = parseInput(input);
    const ring = new LinkedRing(cups);
    const cupAmount = cups.length;

    let currentCup = ring.first;

    for (let i = 0; i < 10; i++) {
      // console.log("ROUND", i + 1, "=====================");
      // console.log("currentCup", currentCup.value);
      // console.log(ring.getValues());
      const endNode = currentCup.getNextNode(3);
      const removedValues = currentCup.getNextValuesSetTo(endNode);

      // console.log(removedValues);

      let destinationCupLabel =
        currentCup.value === 1 ? cupAmount + 1 : currentCup.value - 1;
      // console.log(destinationCupLabel);
      while (
        !ring.getNodeByValue(destinationCupLabel) ||
        removedValues.has(destinationCupLabel)
      ) {
        destinationCupLabel--;
        if (destinationCupLabel === 0) {
          destinationCupLabel = destinationCupLabel + cupAmount;
        }
      }
      // console.log(destinationCupLabel);
      const destinationCup = ring.getNodeByValue(destinationCupLabel);

      ring.moveNodesAfter(currentCup, endNode, destinationCup);

      currentCup = currentCup.next;
    }

    return ring
      .getNodeByValue(1)
      .getNextValuesTo(ring.getNodeByValue(1).getNextNode(cupAmount))
      .join("");
  }

  async solveGold(input) {
    let cups = parseInput(input);

    for (let i = cups.length + 1; i <= 1000000; i++) {
      cups.push(i);
    }
    const ring = new LinkedRing(cups);
    const cupAmount = cups.length;

    let currentCup = ring.first;

    for (let i = 0; i < 10000000; i++) {
      // console.log("ROUND", i + 1, "=====================");
      // console.log("currentCup", currentCup.value);
      // console.log(ring.getValues());
      const endNode = currentCup.getNextNode(3);
      const removedValues = currentCup.getNextValuesSetTo(endNode);

      // console.log(removedValues);

      let destinationCupLabel =
        currentCup.value === 1 ? cupAmount + 1 : currentCup.value - 1;
      // console.log(destinationCupLabel);
      while (
        !ring.getNodeByValue(destinationCupLabel) ||
        removedValues.has(destinationCupLabel)
      ) {
        destinationCupLabel--;
        if (destinationCupLabel === 0) {
          destinationCupLabel = destinationCupLabel + cupAmount;
        }
      }
      // console.log(destinationCupLabel);
      const destinationCup = ring.getNodeByValue(destinationCupLabel);

      ring.moveNodesAfter(currentCup, endNode, destinationCup);

      currentCup = currentCup.next;
    }

    return ring
      .getNodeByValue(1)
      .getNextValuesTo(ring.getNodeByValue(1).getNextNode(2))
      .map((n) => BigInt(n))
      .reduce((a, b) => a * b);
  }
};
