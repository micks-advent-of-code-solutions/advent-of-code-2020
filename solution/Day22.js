const Solution = require("./Solution");

const parseInput = (input) =>
  input.split("\n\n").map((playerData) =>
    playerData
      .split("\n")
      .slice(1)
      .map((n) => parseInt(n))
  );

const Game = (playerDecks) => {
  const playTurn = () => {
    const cards = [];
    playerDecks.forEach((deck, i) => {
      cards.push(deck.shift());
    });
    const biggestCard = Math.max(...cards);
    const winner = cards.indexOf(biggestCard);
    cards
      .sort((a, b) => b - a)
      .forEach((card) => playerDecks[winner].push(card));
  };

  const isGameOver = () => playerDecks.some((deck) => deck.length === 0);

  const play = () => {
    while (!isGameOver()) {
      playTurn();
    }
  };

  return {
    playTurn,
    play,
    isGameOver,
    playerDecks,
  };
};

let globalGameId = 1;
const RecursiveGame = (playerDecks, gameID = 1) => {
  let turn = 0;
  const playedGames = new Set();
  const playTurn = () => {
    turn++;
    const currentGame = JSON.stringify(playerDecks);
    // console.log(
    //   gameID,
    //   turn,
    //   playerDecks.map((d) => d.length)
    // );
    const cards = [];
    playerDecks.forEach((deck, i) => {
      cards.push(deck.shift());
    });

    let winner = 0;

    if (!playedGames.has(currentGame)) {
      if (cards.every((card, player) => card <= playerDecks[player].length)) {
        // Recursive match

        globalGameId++;
        //console.log(`OMG RECURSION --- NEW GAME ${globalGameId}`);
        const newDecks = cards.map((cardCount, player) =>
          playerDecks[player].slice(0, cardCount)
        );
        //console.log(newDecks);
        const recursionGame = RecursiveGame(newDecks, globalGameId);
        recursionGame.play();
        winner = recursionGame.getWinner();
        //console.log("OMG RECURSION --- GAME ENDED");
        //console.log("WINNER", winner);
      } else {
        // Normal round, biggest card wins
        const biggestCard = Math.max(...cards);
        winner = cards.indexOf(biggestCard);
      }
    } else {
      //console.log("DID THAT ONE ALREADY");
      winner = 0;
    }

    const sortedCards = [cards[winner], cards[1 - winner]];

    sortedCards.forEach((card) => playerDecks[winner].push(card));

    playedGames.add(currentGame);
  };

  const isGameOver = () => playerDecks.some((deck) => deck.length === 0);
  const getWinner = () =>
    isGameOver()
      ? playerDecks
          .map((deck, i) => [deck, i])
          .find(([deck]) => deck.length !== 0)[1]
      : null;

  const play = () => {
    while (!isGameOver()) {
      playTurn();
    }
  };

  return {
    playTurn,
    play,
    isGameOver,
    getWinner,
    playerDecks,
  };
};

module.exports = class Day22 extends (
  Solution
) {
  async solveSilver(input) {
    const playerDecks = parseInput(input);
    const game = Game(playerDecks);
    game.play();

    return game.playerDecks
      .find((deck) => deck.length !== 0)
      .map((card, position, arr) => card * (arr.length - position))
      .reduce((a, b) => a + b);
  }

  async solveGold(input) {
    const playerDecks = parseInput(input);
    const game = RecursiveGame(playerDecks);
    game.play();
    return game.playerDecks[game.getWinner()]
      .map((card, position, arr) => card * (arr.length - position))
      .reduce((a, b) => a + b);
  }
};
