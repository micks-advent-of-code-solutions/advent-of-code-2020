const Solution = require("./Solution");

const readInput = (input) =>
  Object.fromEntries(
    input.split("\n").map((rule) => {
      const [bag, inners] = rule.split(" contain ");
      return [
        /([a-z ]+) bag?s/.exec(bag)[1],
        Object.fromEntries(
          inners
            .split(", ")
            .filter((inner) => inner !== "no other bags.")
            .map((inner) => {
              const {
                groups: { amount, name },
              } = /(?<amount>\d+) (?<name>[a-z ]+) bags?.?/.exec(inner);
              return [name, parseInt(amount)];
            })
        ),
      ];
    })
  );

const DependentsSolver = (dependencyMap) => {
  const resolve = (value) => {
    const dependents = Object.entries(dependencyMap)
      .filter(([, dependencies]) => dependencies.hasOwnProperty(value))
      .map(([bag]) => bag);

    if (dependents.length) {
      return [value, ...dependents.map(resolve)];
    } else {
      return value;
    }
  };
  return resolve;
};

const BagCounter = (dependencyMap) => {
  const resolve = (bag, amount) => {
    const dependencies = Object.entries(dependencyMap[bag]);

    if (dependencies.length) {
      return (
        dependencies
          .map(([bag, bagAmount]) => resolve(bag, bagAmount) * amount)
          .reduce((a, b) => a + b) + amount
      );
    } else {
      return amount;
    }
  };
  return resolve;
};

module.exports = class Day7 extends (
  Solution
) {
  async solveSilver(input) {
    const ruleList = readInput(input);

    // build dependency tree:
    const dependents = new Set(
      DependentsSolver(ruleList)("shiny gold").flat(Infinity)
    );
    return dependents.size - 1;
  }

  async solveGold(input) {
    const ruleList = readInput(input);
    return BagCounter(ruleList)("shiny gold", 1) - 1;
  }
};
